var $slider = $('.bxslider').bxSlider();
$slider.bxSlider({
    infiniteLoop: false,
    hideControlOnEnd: true
});

function completeQuestion(id, position) {
    $.post("/api/question/complete", {
            'question_id': id
        },
        function (data) {
            if (data.status == 0) {
                $item = $('.bxslider li:nth-child(' + (position + 1).toString() + ')');
                $item.data('answered', 'True');
                controlAnswerBtn();
                $item.children().last().find('span').removeClass('label-primary');
                $item.children().last().find('span').addClass('label-success');
                $item.children().last().find('span').text('Completed')
            }
        }
    );
}

function addQuestion(data){
    $bxslider = $('.bxslider').append(data);
}

function removeQuestion(id, position) {
    $.post("/api/question/remove", {
            'question_id': id
        },
        function (data) {
            if (data.status != 0) {
                $item = $('.bxslider li:nth-child(' + (position + 1).toString() + ')');
                $item.remove();
                if ($slider.getSlideCount() != 1) {
                    $slider.reloadSlider();
                    if (position != 0) {
                        $slider.goToSlide(position - 1);
                    }
                    onBeforeLoadSlider();
                } else {
                    var no_item = "<li><h1>질문이 없습니다.</h1><p>청중에게 링크를 보내 질문 등록을 요청하세요.<br><a href=\"#\">http://donhesi.com/space/"+ data.status +"</a></p></li>";
                    addQuestion(no_item);
                    $slider.reloadSlider();
                    $slider.goToSlide(0);
                    noItemConfig();
                }
            }
        }
    );
}


function controlNavigationBtn() {
    var current = $slider.getCurrentSlide();
    var total = $slider.getSlideCount() - 1;

    if (current == 0 && current == total) {
        $('.btn-prev').hide();
        $('.btn-next').hide();
    } else {
        $('.btn-prev').show();
        $('.btn-next').show();
        if (current == 0) {
            $('.btn-prev').hide();
        } else if (current == total) {
            $('.btn-next').hide();
        }
    }
}

function controlAnswerBtn() {
    var current = $slider.getCurrentSlide();
    var answered = $('.bxslider li:nth-child(' + (current + 1).toString() + ')').data('answered');

    if (answered == "True") {
        $('.right-bottom-corner').hide();
    } else if (answered == "False") {
        $('.right-bottom-corner').show();
    }
}

function onBeforeLoadSlider() {
    controlNavigationBtn();
    controlAnswerBtn();
}

function noItemConfig(){
    $('.right-bottom-corner').hide();
    $('.left-bottom-corner').hide();
    $('#question-remove').hide();
}

$(document).keydown(function (key) {
    if (!$(key.target).is($('input'))) {
        switch (parseInt(key.which, 10)) {
            case 13:
                $slider.goToNextSlide();
                break;
            case 32:
                $slider.goToNextSlide();
                break;
            case 37:
                $slider.goToPrevSlide();
                break;
            case 39:
                $slider.goToNextSlide();
                break;
        }
        $(".popoverLink").popover('hide');
    }
});

controlNavigationBtn();
controlAnswerBtn();


$(".popoverLink").popover({
    html: true,
    placement: 'top',
    trigger: 'click',
    container: 'body',
    title: function () {
        return $(this).parent().siblings().first().html();
    },
    content: function () {
        return $(this).parent().siblings().first().next().html();
    }
});


$('.question').on('click', function () {
    var page = $(this).data('index');
    $slider.goToSlide(page);
    $('#questionListModal').modal('hide');
});

$('#question-remove').click(function () {
    var cur_question = $slider.getCurrentSlide();
    var question_id = $('.bxslider li:nth-child(' + (cur_question + 1).toString() + ')').data('id');
    if (confirm("이 질문을 삭제하시겠습니까??") == true) {
        removeQuestion(question_id, cur_question);
    }
});

$('#postpone-btn').click(function () {
    $slider.goToNextSlide();
    $(this).blur();
});

$('#complete-btn').click(function () {
    var cur_question = $slider.getCurrentSlide();
    var question_id = $('.bxslider li:nth-child(' + (cur_question + 1).toString() + ')').data('id');
    if (confirm("이 질문에 답변하셨습니까??") == true) {
        $slider.goToNextSlide();
        $('#postpone-btn').blur();
        completeQuestion(question_id, cur_question);
    }
});

$('.btn-prev').click(function () {
    $slider.goToPrevSlide();
    $(".popoverLink").popover('hide');
    $(this).blur();
});

$('.btn-next').click(function () {
    $slider.goToNextSlide();
    $(".popoverLink").popover('hide');
    $(this).blur();
});