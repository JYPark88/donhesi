$(document).ready(function () {
    var $tagsInput = $('.tagsinput'),
        $tagInput = $('.tagsinput-primary input[type=text]'),
        $questionInput = $('.form-control[name=question]'),
        $spaceIdInput = $('input[name=spaceId]');

    function makeNewQuestion() {
        var __tag = $tagsInput.val();
        var __question = $questionInput.val().trim();
        var __space_id = $spaceIdInput.val();

        if (__tag && __question) {
            $.post("/api/question/create", {
                    'space_id': __space_id,
                    'tag': __tag,
                    'question': __question
                },
                function (data) {
                    if (data.status == 0) {
                        $tagsInput.tagsinput('removeAll');
                        $questionInput.val('');
                        alert('등록되었습니다.');
                    }
                }
            );
        } else if (!__tag) {
            alert("태그를 입력해주세요.");
        } else {
            alert("질문 내용을 입력해주세요.");
        }
    }

    $('#question-submit').click(function () {
        makeNewQuestion();
    });

    $tagInput.focus(function () {
        $('.tagsinput-primary .bootstrap-tagsinput').css('border-color', '#45C8AF');
    });

    $tagInput.focusout(function () {
        $('.tagsinput-primary .bootstrap-tagsinput').css('border-color', '#bdc3c7');
    });

    $questionInput.focus(function () {
        $(this).css('border-color', '#3498DB');
        $(this).prev().addClass('username-addon');
    });

    $questionInput.focusout(function () {
        $(this).css('border-color', '#bdc3c7');
        $(this).prev().removeClass('username-addon');
    });
});
