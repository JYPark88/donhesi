function fblogin() {
    var name;
    var fb_id;
    var img_url;
    var link;
    FB.api('/me', function (response) {
        name = response.name;
        fb_id = response.id;
        link = response.link;
        FB.api('/me/picture?width=35&height=35', function (response) {
            img_url = response.data.url;
            $.post("/api/login", {
                    'name' : name,
                    'fb_id' : fb_id,
                    'img_url' : img_url,
                    'link' : link
                },
                function() {
                    location.reload();
                }
            );
        });
    });
}


$(document).ready(function () {
    $('#facebook-btn').click(function () {
        FB.login(function (response) {
            if(response.status == 'connected'){
                fblogin();
            }
        },{scope: 'email'});
    });
});