function spaceValidator() {
    $('#space-form').validate({
        rules: {
            title: {
                required: true,
                minlength: 2
            },
            description: {
                required: true
            },
            confirm_password: {
                equalTo: "#password"
            }
        },
        messages: {
            title: {
                required: "주제를 입력해주세요.",
                minlength: "최소 2글자를 입력해주세요."
            },
            description: {
                required: "설명을 입력해주세요."
            },
            confirm_password: {
                equalTo: "비밀번호가 일치하지 않습니다."
            }
        }
    });
}

$(document).ready(function () {
    var $titleInput = $('.form-control[name=title]'),
        $descriptionInput = $('.form-control[name=description]'),
        $slideUrlInput = $('.form-control[name=slide-url]'),
        $passwordInput = $('.form-control[name=password]');

    var $spaceForm = $('#space-form');

    $('#set-password').click(function () {
        $this = $(this);
        $('.space-password').toggle();
        $this.text(function (i, text) {
            return text === "비밀번호 설정" ? "비밀번호 삭제" : "비밀번호 설정";
        });

        if ($this.hasClass('btn-danger')) {
            $this.removeClass('btn-danger');
            $this.addClass('btn-info');
        } else {
            $this.removeClass('btn-info');
            $this.addClass('btn-danger');
        }
    });

    spaceValidator();

    $('#make-space').click(function () {
        if ($spaceForm.valid()) {
            makeNewSpace();
        }
    });

    function makeNewSpace() {
        var __title = $titleInput.val().trim();
        var __description = $descriptionInput.val().trim();
        var __slide_url = null;
        var __password = $passwordInput.val();

        if ($slideUrlInput.val().trim()) {
            __slide_url = $slideUrlInput.val().trim();
        } else {
            __slide_url = null;
        }

        if (__title && __description) {
            $.post("/api/space/create", {
                    'title': __title,
                    'description': __description,
                    'slideurl': __slide_url,
                    'password': __password
                },
                function (data) {
                    if (data.status == 0) {
                        $('#spaceModal').modal('hide');
                        location.replace('/');
                    }
                }
            );
        }
    }

    $('.panel').hover(
        function () {
            $(this).css('border-color', '#E74C3C');
            $(this).children().first().css('border-color', '#E74C3C');
            $(this).children().first().css('background-color', '#E74C3C');
        },
        function () {
            $(this).css('border-color', '#45C8AF');
            $(this).children().first().css('border-color', '#45C8AF');
            $(this).children().first().css('background-color', '#45C8AF');
        }
    );

    $('.navbar-form input').keydown(function (key) {
        switch (parseInt(key.which, 10)) {
            case 13:
                var value = $(this).val();
                if (isNaN(value)){
                    alert("숫자를 입력해주세요.");
                } else {
                    location.href = "/space/" + value;
                }
                break;
        }
    });


});