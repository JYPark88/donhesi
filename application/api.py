# -*- coding: utf-8 -*-
from application import app, db
from flask import request, jsonify, session, g, redirect, url_for
from werkzeug.security import generate_password_hash,check_password_hash
from sqlalchemy.orm.exc import NoResultFound
from kstime import kstime

from models import (
    User,
    Space,
    Question
)


@app.route('/api/login', methods=['POST'])
def api_login():
    if request.method == "POST":
        data = request.form

        name = data['name']
        fb_id = data['fb_id']
        img_url = data['img_url']
        link = data['link']

        try:
            user = db.session.query(User.id).filter(User.fb_id == fb_id).one()
        except NoResultFound:
            user = User(name=name,
                        fb_id=fb_id,
                        profile_url=img_url,
                        link=link,
                        date=kstime(9)
            )

            db.session.add(user)
            db.session.commit()

        session['username'] = name
        session['user_id'] = user.id

        return jsonify({"status": 0})


@app.route('/api/logout', methods=['GET'])
def api_logout():
    session.clear()

    return redirect(url_for("main"))


@app.route('/api/space/create', methods=['POST'])
def api_create_space():
    if request.method == "POST":
        data = request.form
        title = data['title']
        description = data['description']
        slideurl = data['slideurl']
        password = data['password']

        space = Space(
            user_id=session['user_id'],
            title=title,
            description=description,
            date=kstime(9)
        )

        if slideurl != '':
            space.slide_url=slideurl

        if password != '':
            space.password=generate_password_hash(password)

        db.session.add(space)
        db.session.commit()

        return jsonify({"status": 0})

@app.route('/api/space/modify', methods=['POST'])
def api_modify_space():
    if request.method == "POST":
        data = request.form
        space_id = data['space_id']
        title = data['title']
        description = data['description']
        slideurl = data['slideurl']
        password = data['password']

        space = Space.query.get(space_id)
        space.title = title
        space.description = description

        if slideurl != '':
            space.slide_url=slideurl

        if password != '':
            space.password=generate_password_hash(password)

        db.session.commit()

        return jsonify({"status": 0})

@app.route('/api/space/delete', methods=['POST'])
def api_delete_space():
    if request.method == "POST":
        data = request.form
        space_id = data['space_id']

        space = Space.query.get(space_id)

        if 'user_id' in session and session['user_id'] == space.user_id:
            db.session.delete(space)
            db.session.commit()

            return redirect(url_for('main'))

        else:
            return "Not permitted Access"


@app.route('/api/space/password', methods=['POST'])
def api_space_password():
    if request.method == "POST":
        data = request.form
        id = data['id']
        password = data['password']

        space = Space.query.get(id)

        if check_password_hash(space.password, password):
            session[str(id)] = 1
            return jsonify({"status": 0})
        else:
            return jsonify({"status": 1})


@app.route('/api/question/create', methods=['POST'])
def api_create_question():
    if request.method == "POST":
        data = request.form
        space_id = data['space_id']
        tag = data['tag']
        question = data['question']

        question = Question(
            space_id = space_id,
            user_id = session['user_id'],
            tag = tag,
            question =question,
            date=kstime(9)
        )

        db.session.add(question)
        db.session.commit()

        return jsonify({"status": 0})


@app.route('/api/question/complete', methods=['POST'])
def api_complete_question():
    if request.method == "POST":
        data = request.form
        question_id = data['question_id']

        question = Question.query.get(question_id)
        question.answered = 1

        db.session.commit()

        return jsonify({"status": 0})


@app.route('/api/question/remove', methods=['POST'])
def api_remove_question():
    if request.method == "POST":
        data = request.form
        question_id = data['question_id']

        question = Question.query.get(question_id)

        if 'user_id' in session and session['user_id'] == question.space.user_id:
            db.session.delete(question)
            db.session.commit()

            return jsonify({"status": question.space.id})
        else:
            return jsonify({"status": 0})



@app.before_request
def app_before_request():
    g.user = None
    g.id = None
    if 'username' in session:
        g.user = session['username']
        g.id = session['user_id']