from application import db

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    fb_id = db.Column(db.String(150), unique=True)
    link = db.Column(db.String(255))
    profile_url = db.Column(db.String(255))
    date = db.Column(db.DateTime())


class Space(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', backref=db.backref('spaces', cascade='all, delete-orphan', lazy='dynamic'))
    title = db.Column(db.String(150))
    slide_url = db.Column(db.String(255))
    password = db.Column(db.String(255))
    description = db.Column(db.String(255))
    date = db.Column(db.DateTime())


class Question(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    space_id = db.Column(db.Integer, db.ForeignKey('space.id'))
    space = db.relationship('Space', backref=db.backref('questions', cascade='all, delete-orphan', lazy='dynamic'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', backref=db.backref('questions', cascade='all, delete-orphan', lazy='dynamic'))
    question = db.Column(db.String(255))
    tag = db.Column(db.String(50))
    answered = db.Column(db.Boolean, default=0)
    date = db.Column(db.DateTime())


class Comment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    question_id = db.Column(db.Integer, db.ForeignKey('question.id'))
    question = db.relationship('Question', backref=db.backref('comments', cascade='all, delete-orphan', lazy='dynamic'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', backref=db.backref('comments', cascade='all, delete-orphan', lazy='dynamic'))
    comment = db.Column(db.String(255))
    date = db.Column(db.DateTime())