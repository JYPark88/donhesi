from flask import render_template, session, g, request, redirect, url_for

from application import app, db
from application import api,jinja_func
from sqlalchemy import desc,distinct,func, and_
from sqlalchemy.orm import contains_eager
from models import User, Space, Question, Comment

@app.route('/', methods=['GET', 'POST'])
def main():
    if g.user == None:
        return render_template("index.html")
    else:
        context = {}

        subquery = db.session.query(Question.space_id, func.count('*').label('question_count')).group_by(Question.space_id).subquery()
        context['space_list'] = db.session.query(Space, subquery.c.question_count)\
            .filter(Space.user_id == session['user_id'])\
            .outerjoin(subquery, Space.id==subquery.c.space_id)\
            .order_by(desc(Space.date))

        # context['space_list'] = Space.query.filter(Space.user_id == session['user_id']).order_by(desc(Space.date))
        # print context['space_list']
        # for item in context['space_list']:
        #     print item
        # context['space_list'] = Space.query.all()

        return render_template('main.html', context=context)


@app.route('/space/<int:space_id>', methods=['GET'])
def space(space_id):
    if request.method == 'GET':
        space = Space.query.get(space_id)

        if space is None:
            return render_template('/space/no_space.html')
        else :
            questions = Question.query.join(Question.user).filter(Question.space_id==space_id).order_by(Question.date).options(contains_eager(Question.user)).all()

            if 'user_id' in session and session['user_id'] == space.user_id:
                return render_template('/space/detail_master.html', space = space, questions=questions)
            else:
                if space.password is None:
                    return render_template('/space/detail_user.html', space = space, questions=questions)
                else :
                    if str(space_id) in session:
                        del session[str(space_id)]
                        return render_template('/space/detail_user.html', space = space, questions=questions)
                    else:
                        return render_template('/space/detail_password_check.html', space_id=space_id)

@app.route('/space/<int:space_id>/<order>', methods=['GET'])
def ordered_space(space_id, order):
    if request.method == 'GET':
        space = Space.query.get(space_id)

        if space is None:
            return render_template('/space/no_space.html')
        else:
            if 'user_id' in session and session['user_id'] == space.user_id:
                if order == 'desc':
                    questions = Question.query.join(Question.user).filter(Question.space_id==space_id).order_by(desc(Question.date)).options(contains_eager(Question.user)).all()
                elif order == 'new':
                    questions = Question.query.join(Question.user).filter(and_(Question.space_id==space_id, Question.answered==0)).order_by(desc(Question.date)).options(contains_eager(Question.user)).all()
                elif order == 'completed':
                    questions = Question.query.join(Question.user).filter(and_(Question.space_id==space_id, Question.answered==1)).order_by(desc(Question.date)).options(contains_eager(Question.user)).all()
                else:
                    questions = Question.query.join(Question.user).filter(Question.space_id==space_id).order_by(Question.date).options(contains_eager(Question.user)).all()

                return render_template('/space/detail_master.html', space = space, questions=questions)
            else:
                return redirect(url_for('space', space_id=space_id))


# Error handlers
# Handle 404 errors
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


# Handle 500 errors
@app.errorhandler(500)
def server_error(e):
    return render_template('500.html'), 500
