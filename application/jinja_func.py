from application import app

@app.template_filter('split_tag')
def split_tag_filter(string):
    return string.split(',')